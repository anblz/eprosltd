﻿var ViewModelContacts = {
    firstState: ko.observable(true),
    firstHead: ko.observable(true),
    firstBase: ko.observable(),

    visFirst: function () {
        ViewModelServices.visAll();
        ViewModelServices.firstState(true)
        ViewModelServices.firstHead(true)
    },
    visAll: function () {
        ViewModelContacts.firstState(false)
    }
}
$(document).ready(function () {

    $(".correct").click(function () {
        var editor = CKEDITOR.replace($(this).parent().children().last().attr('id'));
        $('#correct,#delete').prop('disabled', true);
    })

    $("#apply").click(function (e) {
        e.preventDefault();
        var content = {
            "id": 1,
            "content": CKEDITOR.instances.firstState.getData()
        };
        $.ajax({
            url: "/api/contactbase/PutContact?id=1",
            type: "PUT",
            data: JSON.stringify(content),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                alert("Запись обновлена");
                window.location.replace("/Admin/Contact");
            },
            error: function (e) {
                alert("Ошибка добавления")
            }
        })

    })
    function GetAllContacts() {
        $.ajax({
            url: "/api/contactbase/GetContact",
            type: "GET",
            cache: false,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            success: function (data) {
                ViewModelContacts.firstBase(data[0].Content);
            }
        })
    }
    GetAllContacts();
    ko.applyBindings(ViewModelContacts);
})