namespace EprosNew.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.News", "TestStr", c => c.String());
            DropColumn("dbo.ProductCategoriesDTOes", "Tests");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ProductCategoriesDTOes", "Tests", c => c.String());
            DropColumn("dbo.News", "TestStr");
        }
    }
}
