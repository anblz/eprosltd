﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Web;

namespace EprosNew.Controllers
{
    public class imgLoadController : ApiController
    {
        [HttpPost]
        public IHttpActionResult LoadImage()
        {
            var filepath = string.Empty;
            var request = HttpContext.Current.Request;
            var fileName = string.Empty;
            for (var i = 0; i < request.Files.Count; i++)
            {
                var postedFile = request.Files[i];

                fileName = Path.GetFileName(postedFile.FileName);
                var extension = Path.GetExtension(fileName).ToLower();
                string[] excludes = {".jpg",".bmp", ".png",".ink"};
                if (excludes.Any(extension.Contains))
                {
                    postedFile.SaveAs(HttpContext.Current.Server.MapPath("/Content/images/text-images/" + fileName));
                    filepath = "http://eprosltd.ru/Content/images/text-images/" + fileName;
                    return Ok(new { FPath = filepath });
                }
            }
            return BadRequest();
        }
    }
}
