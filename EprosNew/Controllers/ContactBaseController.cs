﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EprosNew.Models;

namespace EprosNew.Controllers
{
    public class ContactBaseController : ApiController
    {
        UsersContext db = new UsersContext();
        public class ContentHelper
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string content { get; set; }
            public int? ParentId { get; set; }
        }
        public IEnumerable<Contact> GetContact()
        {
            return db.Contacts;
        }
        public IHttpActionResult PutContact(int id, [FromBody] ContentHelper ch)
        {
            if (string.IsNullOrEmpty(ch.content))
            {
                return BadRequest("");
            }
            try
            {
                var content = db.Contacts.FirstOrDefault(n => n.Id == ch.Id);
                if (content == null)
                {
                    return BadRequest("");
                }
                else
                {
                    content.Content = ch.content;
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }

    }
}
