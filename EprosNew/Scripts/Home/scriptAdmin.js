﻿$(function () {
    $(".exit").click(function () {
        $.ajax({
            url: "/api/Authentication",
            type: "GET",
            contentType: 'application/json; charset=UTF-8',
            success: function () {
                window.location.replace("/Home/Index");
            },
            error: function (e) {
                alert('При выходе возникла ошибка' + e.responseText);

            }
        })
    })
})