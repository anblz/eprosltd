﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EprosNew.Models;

namespace DbInitilizer
{
    class DropCreateDbInitializer : DropCreateDatabaseAlways<UsersContext>
    {
        protected override void Seed(UsersContext context)
        {
            context.User.AddRange(InititalDbContent.UsersList);
            context.SaveChanges();

            context.Product.AddRange(InititalDbContent.ProductList);
            context.SaveChanges();

            context.New.AddRange(InititalDbContent.NewsList);
            context.SaveChanges();

            context.Service.AddRange(InititalDbContent.ServiceList);
            context.SaveChanges();

            context.Contacts.AddRange(InititalDbContent.ContactList);
            context.SaveChanges();
        }
    }
}
