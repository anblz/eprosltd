﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EprosNew.Models.DTO
{
    public class ServiceDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<TreeService> TreeService
        {
            get; set;
        }
    }
}