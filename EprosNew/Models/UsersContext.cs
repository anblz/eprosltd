﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;

namespace EprosNew.Models
{
    public class UsersContext : DbContext
    {
        public DbSet<Users> User { get; set; }
        public DbSet<ProductCategoriesDTO> Product { get; set; }
        public DbSet<ServiceDTO> Service { get; set; }
        public DbSet<AboutDTO> Abouts { get; set; }
        public DbSet<Contact> Contacts { get; set; }
        public DbSet<News> New { get; set; }
        public UsersContext()
        : base("Epros")
        { }
    }
}