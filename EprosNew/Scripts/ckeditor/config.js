/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
    config.skin = 'office2013'
    config.allowedContent = true;
    config.extraPlugins = 'fileupload,divarea';
    config.toolbar = 'NewToolBar';
    config.toolbar_NewToolBar = [['File'],['Bold', 'Italic', 'Underline', 'Font',  'Strikethrough', 'RemoveFormat',  'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock','Table','Undo','Redo','Maximize']];
    config.height = 'calc(100vh - 290px)';
};
