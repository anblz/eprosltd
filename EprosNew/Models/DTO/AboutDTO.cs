﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EprosNew.Models.DTO
{
    public class AboutDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<TreeAbout> TreeAbout
        {
            get; set;
        }
    }
}