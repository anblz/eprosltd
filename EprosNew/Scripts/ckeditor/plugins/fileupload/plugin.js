﻿CKEDITOR.plugins.add('fileupload', {
    icons: 'file',
    init: function (editor) {
        editor.addCommand('fileupload', new CKEDITOR.dialogCommand('fileDialog'));
        editor.ui.addButton('File', {
            label: 'Загрузить картинку',
            command: 'fileupload',
            toolbar: 'insert'
        });
        CKEDITOR.dialog.add('fileDialog', this.path + 'dialogs/fileDialog.js');
    }
});