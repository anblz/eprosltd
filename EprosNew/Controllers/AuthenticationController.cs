﻿using EprosNew.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Results;
using System.Web.Security;

namespace EprosNew.Controllers
{

    public class AuthenticationController : ApiController
    {
        public class UserHelper{
            public string login { get; set; }
            public string password { get; set; }

            }
        
        //GET
        [HttpGet]
        public IHttpActionResult Get()
        {
            try
            {
                FormsAuthentication.SignOut();
                return Ok();
            }
            catch(Exception e)
            {
                return BadRequest(e.Message);
            }
        }
        // POST: api/Authentication

        [HttpPost]
        public IHttpActionResult Post([FromBody]UserHelper user)
        {
            if (string.IsNullOrEmpty(user.login) || string.IsNullOrEmpty(user.password))
            {
                return BadRequest("");
            }
            try
            {
                // для теста сделать полную перезагрузку страницы
                using (var a = new UsersContext())
                {
                
                    var admins = a.User.FirstOrDefault(n => n.Name == user.login && n.Password == user.password);
                    if (admins == null)
                    {
                        return BadRequest("");
                    }
                    else
                    {
                        FormsAuthentication.SetAuthCookie(user.login, false);
                       
                        return Ok();
                    }

                }
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }
    }
}
