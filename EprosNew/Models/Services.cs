﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EprosNew.Models
{
    public class ServiceDTO
    {
        public ServiceDTO()
        {
            TreeService = new List<TreeService>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public int? ParentId { get; set; }
        public IList<TreeService> TreeService
        {
            get; set;
        }
    }
}