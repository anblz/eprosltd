﻿namespace EprosNew.Models.DTO
{
    using System.Collections.Generic;

    public class ProductCategoriesDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public List<TreeProduct> TreeProduct
        {
            get;set;
        }

    }
}