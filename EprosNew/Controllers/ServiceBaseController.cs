﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EprosNew.Models;

namespace EprosNew.Controllers
{
    public class ServiceBaseController : ApiController
    {
        UsersContext db = new UsersContext();
        public class ContentHelper
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string content { get; set; }
            public int? ParentId { get; set; }
        }
        public IHttpActionResult GetService(int id)
        {
            try
            {
                var product = db.Service.FirstOrDefault(i => i.Id == id);
                if (product == null)
                {
                    return BadRequest("");
                }
                else
                {
                    return Ok(product);
                }

            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }
        public IHttpActionResult GetServiceCategories()
        {
            using (var c = new UsersContext())
            {
                var freeItems = c.Service
                         .Where(f => f.ParentId == null)
                         .ToList()
                         .Select(
                             f => new ServiceDTO
                             {
                                 Id = f.Id,
                                 Name = f.Name,
                                 TreeService = c.Service
                                             .Where(p => p.ParentId == f.Id)
                                             .ToList()
                                             .Select(u => new TreeService()
                                             {
                                                 Id = u.Id,
                                                 Name = u.Name
                                             }).ToList()

                             }).ToList();


                return Ok(freeItems);
            }
        }
        [HttpPost]
        public IHttpActionResult PostService([FromBody]ContentHelper ch)
        {

            if (string.IsNullOrEmpty(ch.Name))
            {
                return BadRequest("");
            }

            using (var c = new UsersContext())
            {
                try
                {
                    if (ch.ParentId != null)
                    {
                        var Service = new ServiceDTO
                        {
                            Name = ch.Name,
                            ParentId = ch.ParentId
                        };
                        c.Service.Add(Service);
                        c.SaveChanges();
                        return Ok();
                    }
                    else
                    {
                        var Service = new ServiceDTO
                        {
                            Name = ch.Name
                        };
                        c.Service.Add(Service);
                        c.SaveChanges();
                        return Ok();
                    }

                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }


        }
        [HttpDelete]
        public IHttpActionResult Delete (int id) {
            if (id == 0)
            {
                return BadRequest("");
            }
            using (var c = new UsersContext())
            {
                try
                {
                    var item = c.Service.FirstOrDefault(f => f.Id == id);
                    if (item == null)
                    {
                        return BadRequest("");
                    }
                    else
                    {          
                        c.Service.Remove(item);
                        c.SaveChanges();
                        return Ok();
                    }

                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }

        }
        public IHttpActionResult PutService(int id, [FromBody] ContentHelper ch)
        {
            if (string.IsNullOrEmpty(ch.content))
            {
                return BadRequest("");
            }
            try
            {
                var content = db.Service.FirstOrDefault(n => n.Id == ch.Id);
                if (content == null)
                {
                    return BadRequest("");
                }
                else
                {
                    content.Content = ch.content;
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }

    }
}
