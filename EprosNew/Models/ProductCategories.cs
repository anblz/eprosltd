﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EprosNew.Models
{
    public class ProductCategoriesDTO
    {

        public ProductCategoriesDTO()
        {
            TreeProduct = new List<TreeProduct>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public int? ParentId { get; set; }
        public IList<TreeProduct> TreeProduct
        {
            get;set;
        }

    }
}