﻿var ViewModel = {
    content: ko.observable(),
    state: ko.observable(),
    Ides: ko.observable(),
    vis: function (data, event) {
        var idNow = data.id
        $("#state").css("opacity", "1")
        event.preventDefault();
        $.ajax({
            url: "/api/servicebase/GetService?id=" + idNow,
            type: "GET",
            success: function (data) {
                ViewModel.content(data.Content)
                ViewModel.state(data.Name)
                ViewModel.Ides(data.Id)
            }
        })
    }
}
function GetAllServices() {
    var self = this;
    $.ajax({
        url: "/api/servicebase/GetServiceCategories",
        type: "GET",
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8',
        success: function (data) {
            var arrRes = [];
            data.forEach(function (item, i, data) {
                if (item.TreeService.length == 0)
                    arrRes.push({
                        id: item.Id,
                        text: item.Name
                    });
                else {
                    var nodesTemp = item.TreeService;
                    var nodesToView = [];
                    nodesTemp.forEach(function (node, j, nodesTemp) {
                        nodesToView.push({
                            text: node.Name,
                            id: node.Id
                        })
                    })
                    arrRes.push({
                        id: item.Id,
                        text: item.Name,
                        nodes: nodesToView
                    });
                }
            });
            $('#tree').treeview({
                data: arrRes,
                onNodeSelected: function (event, data) {
                    ViewModel.vis(data, event);
                },
                selectedBackColor: "#00c2f6"
            });
        }
    })
}
$(function () {
    GetAllServices();
    ko.applyBindings(ViewModel);
})
