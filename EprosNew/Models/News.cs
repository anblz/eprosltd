﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EprosNew.Models
{
    public class News
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public string TestStr { get; set; }
        public string Date { get; set; }
    }
}