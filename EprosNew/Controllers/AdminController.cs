﻿using System.Web.Mvc;

namespace EprosNew.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        [Authorize]
        public ActionResult Product()
        {
            return View();
        }
        [Authorize]
        public ActionResult Service()
        {
            return View();
        }
        [Authorize]
        public ActionResult Contact()
        {
            return View();
        }
        [Authorize]
        public ActionResult About()
        {
            return View();
        }
        [Authorize]
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult News()
        {
            return View();
        }
       
    }
}