﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EprosNew.Models
{
    public class AboutDTO
    {
        public AboutDTO()
        {
            TreeAbout = new List<TreeAbout>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public string Content { get; set; }
        public int? ParentId { get; set; }
        public IList<TreeAbout> TreeAbout
        {
            get; set;
        }
    }
}