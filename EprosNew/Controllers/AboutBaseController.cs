﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EprosNew.Models;

namespace EprosNew.Controllers
{
    public class AboutBaseController : ApiController
    {
        UsersContext db = new UsersContext();
        public class ContentHelper
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string content { get; set; }
            public int? ParentId { get; set; }
        }
        public IHttpActionResult GetAbout(int id)
        {
            try
            {
                var product = db.Abouts.FirstOrDefault(i => i.Id == id);
                if (product == null)
                {
                    return BadRequest("");
                }
                else
                {
                    return Ok(product);
                }

            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }
        public IHttpActionResult GetAboutCategories()
        {
            using (var c = new UsersContext())
            {
                var freeItems = c.Abouts
                    .Where(f => f.ParentId == null)
                    .ToList()
                    .Select(
                        f => new AboutDTO
                        {
                            Id = f.Id,
                            Name = f.Name,
                            TreeAbout = c.Abouts
                                        .Where(p => p.ParentId == f.Id)
                                        .ToList()
                                        .Select(u => new TreeAbout()
                                        {
                                            Id = u.Id,
                                            Name = u.Name
                                        }).ToList()

                        }).ToList();


                return Ok(freeItems);
            }
        }
        [HttpPost]
        public IHttpActionResult PostAbout([FromBody]ContentHelper ch)
        {

            if (string.IsNullOrEmpty(ch.Name))
            {
                return BadRequest("");
            }

            using (var c = new UsersContext())
            {
                try
                {
                    if (ch.ParentId != null)
                    {
                        var About = new AboutDTO
                        {
                            Name = ch.Name,
                            ParentId = ch.ParentId
                        };
                        c.Abouts.Add(About);
                        c.SaveChanges();
                        return Ok();
                    }
                    else
                    {
                        var About = new AboutDTO
                        {
                            Name = ch.Name
                        };
                        c.Abouts.Add(About);
                        c.SaveChanges();
                        return Ok();
                    }

                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }


        }
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest("");
            }
            using (var c = new UsersContext())
            {
                try
                {
                    var item = c.Abouts.FirstOrDefault(f => f.Id == id);
                    if (item == null)
                    {
                        return BadRequest("");
                    }
                    else
                    {
                        c.Abouts.Remove(item);
                        c.SaveChanges();
                        return Ok();
                    }

                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }

        }
        public IHttpActionResult PutAbout(int id, [FromBody] ContentHelper ch)
        {
            if (string.IsNullOrEmpty(ch.content))
            {
                return BadRequest("");
            }
            try
            {
                var content = db.Abouts.FirstOrDefault(n => n.Id == ch.Id);
                if (content == null)
                {
                    return BadRequest("");
                }
                else
                {
                    content.Content = ch.content;
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }
    }
}
