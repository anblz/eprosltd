﻿
var ViewModel = {
    content: ko.observable(),
    state: ko.observable(),
    Ides: ko.observable(),
    vis: function (data, event) {
        var idNow = data.id
        event.preventDefault();
        $.ajax({
            url: "/api/productbase/GetProducts?id=" + idNow,
            type: "GET",
            success: function (data) {
                ViewModel.content(data.Content)
                ViewModel.state(data.Name)
                ViewModel.Ides(data.Id)
            }
        })
    }
}
function GetAllProducts() {
    var self = this;

    $.ajax({
        url: "/api/productbase/GetProductsCategories",
        type: "GET",
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8',
        success: function (data) {
            var arrRes = [];
            data.forEach(function (item, i, data) {
                if (item.TreeProduct.length == 0)
                    arrRes.push({
                        id: item.Id,
                        text: item.Name
                        //nodeLevel: 0
                    });
                else {
                    var nodesTemp = item.TreeProduct;
                    var nodesToView = [];
                    nodesTemp.forEach(function (node, j, nodesTemp) {
                        nodesToView.push({
                            text: node.Name,
                            id: node.Id
                            //nodeLevel: 1
                        })
                    })
                    arrRes.push({
                        id: item.Id,
                        text: item.Name,
                        //nodeLevel: 0,
                        nodes: nodesToView
                    });
                }

            });
            $('#tree').treeview({
                data: arrRes,
                onNodeSelected: function (event, data) {
                    ViewModel.vis(data, event);
                },
                selectedBackColor: "#00c2f6"
            });
        }
    })

}
$(function () {
    GetAllProducts();
    ko.applyBindings(ViewModel);
})