﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EprosNew.Models
{
    public class TreeService
    {
        public int? Id { get; set; }
        public string Name { get; set; }
    }
}