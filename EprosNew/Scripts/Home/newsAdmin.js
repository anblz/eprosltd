﻿var ViewModel = {
    content: ko.observable(),
    state: ko.observable(),
    ides: ko.observable(),
    news: ko.observableArray([
    ]),
    correct: function (data, event) {
        var cont = 'state' + data.Id;
        var editor = CKEDITOR.replace(cont);
        $('#correct,#delete').prop('disabled', true);
    },
    save: function (data, event) {
        event.preventDefault();
        var cont = 'state' + data.Id;
        var content = {
            "id": data.Id,
            "content": CKEDITOR.instances[cont].getData()
        };
        $.ajax({
            url: "/api/newsbase/PutNews?id=" + data.Id,
            type: "PUT",
            data: JSON.stringify(content),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                window.location.replace("/Admin/News");
            },
            error: function (e) {
                alert("Ошибка добавления")
            }
        })
    },
    delete: function (data, event) {
        event.preventDefault();
        $.ajax({
            url: "/api/newsbase/Delete?id=" + data.Id,
            type: "DELETE",
            success: function (data) {
                window.location.replace("/Admin/News");
            },
            error: function (e) {
                alert("Произошла ошибка удаления")
            }
        })
    },
    add: function (data,event) {
        event.preventDefault();
        var self = this;
        var prod = {
            "Name": $('#inputName').val(),
            "Date": $('#inputDate').val()
        };
        $.ajax({
            url: "/api/newsbase?="+data.Id,
            type: "POST",
            data: JSON.stringify(prod),
            contentType: 'application/json; charset=UTF-8',
            success: function (data) {
                window.location.replace("/Admin/News");
            },
            error: function (e) {
                alert("Произошла ошибка добавления")
            }

        })
    }
}
function GetAll() {
    $.ajax({
        url: "/api/newsbase/GetNewsNames",
        type: "GET",
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8',
        success: function (data) {
            ViewModel.news(data)
        },
        error: function (e) {

        }
    })
}
$(function () {
    GetAll();
    ko.applyBindings(ViewModel);
})