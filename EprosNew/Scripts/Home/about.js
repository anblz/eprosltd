﻿var ViewModel = {
    content: ko.observable(),
    state: ko.observable(),
    Ides: ko.observable(),
    vis: function (data, event) {
        var idNow = data.id
        event.preventDefault();
        $.ajax({
            url: "/api/aboutbase/GetAbout?id=" + idNow,
            type: "GET",
            success: function (data) {
                ViewModel.content(data.Content)
                ViewModel.state(data.Name)
                ViewModel.Ides(data.Id)
            }
        })
    }
}
function GetAll() {
    $.ajax({
        url: "/api/aboutbase/GetAboutCategories",
        type: "GET",
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8',
        success: function (data) {
            var arrRes = [];
            data.forEach(function (item, i, data) {
                if (item.TreeAbout.length == 0)
                    arrRes.push({
                        id: item.Id,
                        text: item.Name
                    });
                else {
                    var nodesTemp = item.TreeAbout;
                    var nodesToView = [];
                    nodesTemp.forEach(function (node, j, nodesTemp) {
                        nodesToView.push({
                            text: node.Name,
                            id: node.Id
                        })
                    })
                    arrRes.push({
                        id: item.Id,
                        text: item.Name,
                        nodes: nodesToView
                    });
                }
            });
            $('#tree').treeview({
                data: arrRes,
                onNodeSelected: function (event, data) {
                    ViewModel.vis(data, event);
                },
                selectedBackColor: "#00c2f6"
            });
        }
    })
}

$(document).ready(function () {
    GetAll();
    ko.applyBindings(ViewModel);
});
