﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using EprosNew.Models;

namespace EprosNew.Controllers
{
    public class NewsBaseController : ApiController
    {
        UsersContext db = new UsersContext();
        public class ContentHelper
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Content { get; set; }
            public string Date { get; set; }
        }
        [HttpPost]
        public IHttpActionResult PostNews([FromBody]ContentHelper ch)
        {

            if (string.IsNullOrEmpty(ch.Name))
            {
                return BadRequest("");
            }

            using (var c = new UsersContext())
            {
                try
                {
                    var News = new News
                    {
                        Name = ch.Name,
                        Date = ch.Date
                    };
                    c.New.Add(News);
                    c.SaveChanges();
                    return Ok();
                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }


        }
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest("");
            }
            using (var c = new UsersContext()) {
                try
                {
                    var item = c.New.FirstOrDefault(k => k.Id == id);
                    if (item == null)
                    {
                        return BadRequest("");
                    }
                    else
                    {
                        c.New.Remove(item);
                        c.SaveChanges();
                        return Ok();
                    }
                }
                catch (Exception e)
                {

                    return BadRequest(e.Message);
                }
            }


        }
        public IHttpActionResult GetNews(int id)
        {
            try
            {
                var product = db.New.FirstOrDefault(i => i.Id == id);
                if (product == null)
                {
                    return BadRequest("");
                }
                else
                {
                    return Ok(product);
                }

            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }
        public IHttpActionResult GetNewsNames()
        {
            using (var c = new UsersContext())
            {
                var freeItems = c.New
                    .ToList()
                    .Select(
                        f => new News
                        {
                            Id = f.Id,
                            Name = f.Name,
                            Date = f.Date,
                            Content = f.Content

                        }).ToList();

                return Ok(freeItems);
            }
        }
        public IHttpActionResult PutNews(int id, [FromBody] ContentHelper ch)
        {
            if (string.IsNullOrEmpty(ch.Content))
            {
                return BadRequest("");
            }
            try
            {
                var content = db.New.FirstOrDefault(n => n.Id == ch.Id);
                if (content == null)
                {
                    return BadRequest("");
                }
                else
                {
                    content.Content = ch.Content;
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }
    }
}
