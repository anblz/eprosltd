﻿using EprosNew.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;

namespace EprosNew.Controllers
{
    public class ProductBaseController : ApiController
    {
        UsersContext db = new UsersContext();
        public class ContentHelper
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string content { get; set; }
            public int? ParentId { get; set; }
        }
        //Запросы на продукты
        public IHttpActionResult GetProducts(int id)
        {
            try
            {
                var product = db.Product.FirstOrDefault(i => i.Id == id);
                if (product == null)
                {
                    return BadRequest("");
                }
                else
                {
                    return Ok(product);
                }

            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }

        public IHttpActionResult GetProductsCategories()
        {
            using (var c = new UsersContext())
            {
                var freeItems = c.Product
                    .Where(f => f.ParentId == null)
                    .ToList()
                    .Select(
                        f => new ProductCategoriesDTO
                        {
                            Id = f.Id,
                            Name = f.Name,
                            TreeProduct = c.Product
                                        .Where(p => p.ParentId == f.Id)
                                        .ToList()
                                        .Select(u => new TreeProduct()
                                        {
                                            Id = u.Id,
                                            Name = u.Name
                                        }).ToList()

                        }).ToList();


                return Ok(freeItems);
            }
        }
        //Post запрос на добавление элемента
        [HttpPost]
        public IHttpActionResult PostProduct([FromBody]ContentHelper ch)
        {
            
                if (string.IsNullOrEmpty(ch.Name))
                {
                    return BadRequest("");
                }
                
                using (var c = new UsersContext())
                {
                    try
                    {
                    if(ch.ParentId != null)
                    {
                        var prod = new ProductCategoriesDTO
                        {
                            Name = ch.Name,
                            ParentId = ch.ParentId
                        };
                        c.Product.Add(prod);
                        c.SaveChanges();
                        return Ok();
                    }
                    else
                    {
                        var prod = new ProductCategoriesDTO
                        {
                            Name = ch.Name
                        };
                        c.Product.Add(prod);
                        c.SaveChanges();
                        return Ok();
                    }
                        
                    }
                    catch (Exception e)
                    {
                        return BadRequest(e.Message);
                    }
                }
                
            
        }
        //Удаление элемента
        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            if (id == 0)
            {
                return BadRequest("");
            }
            using (var c = new UsersContext())
            {
                try
                {
                    var item = c.Product.FirstOrDefault(f => f.Id == id);
                    if (item == null)
                    {
                        return BadRequest("");
                    }
                    else
                    {
                        c.Product.Remove(item);
                        c.SaveChanges();
                        return Ok();
                    }

                }
                catch (Exception e)
                {
                    return BadRequest(e.Message);
                }
            }

        }

        //api/PUT
        public IHttpActionResult PutProduct(int id, [FromBody] ContentHelper ch)
        {
            if (string.IsNullOrEmpty(ch.content))
            {
                return BadRequest("");
            }
            try
            {
                var content = db.Product.FirstOrDefault(n => n.Id == id);
                if (content == null)
                {
                    return BadRequest("");
                }
                else
                {
                    content.Content = ch.content;
                    db.SaveChanges();
                    return Ok();
                }
            }
            catch (Exception e)
            {

                return BadRequest(e.Message);
            }
        }
    }
}
