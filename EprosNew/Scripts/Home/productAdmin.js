﻿
var ViewModel = {
    content: ko.observable(),
    state: ko.observable(),
    Ides: ko.observable(),
    vis: function (data, event) {
        var idNow = data.id;
        $('.apply, .correct').show();
        $.ajax({
            url: "/api/productbase/GetProducts?id=" + idNow,
            type: "GET",
            success: function (data) {
                ViewModel.content(data.Content)
                ViewModel.state(data.Name)
                ViewModel.Ides(data.Id)
                $('#correct').prop('disabled', false)
                if (document.getElementById('cke_state') != null)
                    var closeCK = CKEDITOR.instances['state'].destroy();
            }
        })
    },
    correct: function (data, event) {
        var editor = CKEDITOR.replace('state');
        $('#correct,#delete').prop('disabled', true);
    },
    save: function (data, event) {
        event.preventDefault();
        var item = $("#state").attr('class');
        var content = {
            "id": item,
            "content": CKEDITOR.instances['state'].getData()
        };
        $.ajax({
            url: "/api/productbase/PutProduct?id=" + item,
            type: "PUT",
            data: JSON.stringify(content),
            contentType: "application/json;charset=utf-8",
            success: function (data) {
                window.location.replace("/Admin/Product");
            },
            error: function (e) {
                alert("Ошибка добавления")
            }
        })
    },
    add: function (data, event) {
        event.preventDefault();
        var self = this;
        var prod = {
            "Name": $('#inputName').val()
        };
        var state = $('#tree').data('treeview').getSelected();
        if (state == false) {
            $.ajax({
                url: "/api/productbase",
                type: "POST",
                data: JSON.stringify(prod),
                contentType: 'application/json; charset=UTF-8',
                success: function (data) {
                    window.location.replace("/Admin/Product");
                },
                error: function (e) {
                    alert("Произошла ошибка добавления")
                }

            })
        }
        else {
            prod =
                {
                    "Name": $('#inputName').val(),
                    "ParentId": state[0].id
                }
            $.ajax({
                url: "/api/productbase",
                type: "POST",
                data: JSON.stringify(prod),
                contentType: 'application/json; charset=UTF-8',
                success: function (data) {
                    window.location.replace("/Admin/Product");
                },
                error: function (e) {
                    alert("Произошла ошибка добавления")
                }

            })
        }
    },
    del: function (data, event) {
        event.preventDefault();
        var id = $('#state').attr('class');
        $.ajax({
            url: "/api/productbase/Delete?id=" + id,
            type: "DELETE",
            success: function (data) {
                window.location.replace("/Admin/Product");
            },
            error: function (e) {
                alert("Произошла ошибка удаления")
            }
        })
    }
}
function updateValue(id, value) {
    // this gets called from the popup window and updates the field with a new value 
    document.getElementById(id).value = value;
}
function GetAllProducts() {
    var self = this;

    $.ajax({
        url: "/api/productbase/GetProductsCategories",
        type: "GET",
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8',
        success: function (data) {
            var arrRes = [];
            data.forEach(function (item, i, data) {
                if (item.TreeProduct.length == 0)
                    arrRes.push({
                        id: item.Id,
                        text: item.Name
                    });
                else {
                    var nodesTemp = item.TreeProduct;
                    var nodesToView = [];
                    nodesTemp.forEach(function (node, j, nodesTemp) {
                        nodesToView.push({
                            text: node.Name,
                            id: node.Id
                        })
                    })
                    arrRes.push({
                        id: item.Id,
                        text: item.Name,
                        nodes: nodesToView
                    });
                }

            });
            $('#tree').treeview({
                data: arrRes,
                onNodeSelected: function (event, data) {
                    ViewModel.vis(data, event);
                },
                selectedBackColor: "#00c2f6"
            });
        }
    })

}

$(function () {
    $('.apply, .correct').hide();
    GetAllProducts();
    ko.applyBindings(ViewModel);

})