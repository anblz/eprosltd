﻿var ViewModel = {
    content: ko.observable(),
    state: ko.observable(),
    ides: ko.observable(),
    news: ko.observableArray([
    ]),
    vis: function (data, event) {
        var idNow = data.Id;
        event.preventDefault();
        $.ajax({
            url: "/api/newsbase/GetNews?id=" + idNow,
            type: "GET",
            success: function (data) {
                ViewModel.content(data.Content)
                ViewModel.state(data.Name)
                ViewModel.ides(data.Id)
            }
        })
    }
}
function GetAll() {
    $.ajax({
        url: "/api/newsbase/GetNewsNames",
        type: "GET",
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8',
        success: function (data) {
            ViewModel.news(data)
        },
        error: function (e) {

        }
    })
}
$(function () {
    GetAll();
    ko.applyBindings(ViewModel);
})