﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EprosNew.Models;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.Data.Entity.Validation;

namespace DbInitilizer
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Инициализатор БД EprosLtd. Доступные действия:");
            var db = new UsersContext();
            Console.WriteLine("1.Инициализировать базу данных(c удалением старого экземпляра)");
            Console.WriteLine("2.Обновить БД через миграции");
            string n = Console.ReadLine();
            if (n == "1")
            {
                try
                {
                    var initializer = new DropCreateDbInitializer();

                    Database.SetInitializer(initializer);

                    using (var context = new UsersContext())
                    {
                        initializer.InitializeDatabase(context);
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString() + " (" +
                                        "Проверьте наличие строки подключения к SQL Server в проекте Epros.DbInitializer в App.config " +
                                        ")");
                }
                Console.ReadKey();
            }
            else if (n == "2")
            {
                var migrator = new MigrateDatabaseToLatestVersion<UsersContext, EprosNew.Migrations.Configuration>();

                migrator.InitializeDatabase(new UsersContext());
            }
            Console.ReadLine();
        }
    }
}
