﻿CKEDITOR.dialog.add('fileDialog', function (editor) {
    return {
        title: 'Загрузка документа',
        minWidth: 400,
        minHeight: 200,
        contents: [
            {
                id: 'tab-basic',
                label: 'Основное',
                elements: [
                    {
                        type: 'file',
                        id: 'file1',
                        validate: CKEDITOR.dialog.validate.notEmpty("Не выбран файл для загрузки.")
                    }
                ]
            }
        ],
        onOk: function () {
            var dialog = this;
            var file = dialog.getContentElement('tab-basic', 'file1').getInputElement().$.files[0];
            var data = new FormData();
            data.append("file", file);
            //var img = editor.document.createElement('img');
            //var link = editor.document.createElement('a');
            //link.setAttribute('href', 'google.com');
            //link.setAttribute('target', "blank")
            //link.setAttribute('class', '');
            //link.setText('ImageTestText');
            //editor.insertElement(link);
            //img.appendTo(link);

            $.ajax({
                type: "POST",
                url: "/api/imgLoad",
                contentType: false,
                processData: false,
                async: false,
                data: data,
                success: function (result) {
                    var path = result.FPath;
                    var img = editor.document.createElement('img');
                    var link = editor.document.createElement('a');
                    img.setAttribute('src', path);
                    img.setAttribute('class', 'pull-right');
                    img.setAttribute('style', 'width:30%; margin:20px;');
                    link.setAttribute('data-fancybox', 'images');
                    link.setAttribute('href', path);
                    link.setAttribute('target', "blank")
                    link.setAttribute('class', '');
                    //link.setText('');
                    editor.insertElement(link);
                    img.appendTo(link);
                },
                error: function (ex) {
                    alert('Неверный формат файла')
                }
            });
        }
    };
});

