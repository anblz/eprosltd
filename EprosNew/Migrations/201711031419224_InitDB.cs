namespace EprosNew.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AboutDTOes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Content = c.String(),
                        ParentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TreeAbouts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        AboutDTO_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AboutDTOes", t => t.AboutDTO_Id)
                .Index(t => t.AboutDTO_Id);
            
            CreateTable(
                "dbo.Contacts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Content = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.News",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Content = c.String(),
                        Date = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ProductCategoriesDTOes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Content = c.String(),
                        ParentId = c.Int(),
                        Tests = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TreeProducts",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ProductCategoriesDTO_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ProductCategoriesDTOes", t => t.ProductCategoriesDTO_Id)
                .Index(t => t.ProductCategoriesDTO_Id);
            
            CreateTable(
                "dbo.ServiceDTOes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Content = c.String(),
                        ParentId = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.TreeServices",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        ServiceDTO_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.ServiceDTOes", t => t.ServiceDTO_Id)
                .Index(t => t.ServiceDTO_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Password = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TreeServices", "ServiceDTO_Id", "dbo.ServiceDTOes");
            DropForeignKey("dbo.TreeProducts", "ProductCategoriesDTO_Id", "dbo.ProductCategoriesDTOes");
            DropForeignKey("dbo.TreeAbouts", "AboutDTO_Id", "dbo.AboutDTOes");
            DropIndex("dbo.TreeServices", new[] { "ServiceDTO_Id" });
            DropIndex("dbo.TreeProducts", new[] { "ProductCategoriesDTO_Id" });
            DropIndex("dbo.TreeAbouts", new[] { "AboutDTO_Id" });
            DropTable("dbo.Users");
            DropTable("dbo.TreeServices");
            DropTable("dbo.ServiceDTOes");
            DropTable("dbo.TreeProducts");
            DropTable("dbo.ProductCategoriesDTOes");
            DropTable("dbo.News");
            DropTable("dbo.Contacts");
            DropTable("dbo.TreeAbouts");
            DropTable("dbo.AboutDTOes");
        }
    }
}
