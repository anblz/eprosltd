﻿var ViewModelContacts = {
    firstState: ko.observable(true),
    firstHead: ko.observable(true),
    firstBase: ko.observable(),

    visFirst: function () {
        ViewModelServices.visAll();
        ViewModelServices.firstState(true)
        ViewModelServices.firstHead(true)
    },
    visAll: function () {
        ViewModelContacts.firstState(false)
    }
}
$(document).ready(function () {
    function GetAllContacts() {
        $.ajax({
            url: "/api/contactbase/GetContact",
            type: "GET",
            cache: false,
            dataType: 'json',
            contentType: 'application/json; charset=UTF-8',
            success: function (data) {
                ViewModelContacts.firstBase(data[0].Content);
            }
        })
    }
    GetAllContacts();
    ko.applyBindings(ViewModelContacts);
})