﻿var View = {
    news: ko.observableArray([
    ])
}
function Get() {
    $.ajax({
        url: "/api/newsbase/GetNewsNames",
        type: "GET",
        cache: false,
        dataType: 'json',
        contentType: 'application/json; charset=UTF-8',
        success: function (data) {
		data.forEach(function(item, i, data) {
            $("#footer-news").append("<li><a href='/Home/News' id='link-news'>"+item.Name+"</a></li>");
        });
		},
        error: function (e) {
        }
    })
}
$(function () {
    //Прозрачность
    var $menu = $('.navbar');
    var menu_bgShowed = false;
    var menu_nobgTopPx = 0;
    var menu_maxBgOpacity = .85;
    var headerHeight = $('.navbar').height() - menu_nobgTopPx;
    var colorRGB1 = [0, 154, 255].join();
    var colorRGB2 = [4, 73, 138].join();
    var colorRGB3 = [4, 73, 138].join();
    var colorRGB4 = [0, 154, 255].join();

    function fadeMenuBg() {
        var scrollX = $(this).scrollTop() - menu_nobgTopPx;

        if (scrollX > headerHeight) {
            if (!menu_bgShowed) {
                menu_bgShowed = true;
                var _color = 'linear-gradient(to right, rgba(' + colorRGB1 + ', ' + menu_maxBgOpacity + ') 0%, rgba(' + colorRGB2 + ', ' + menu_maxBgOpacity + ') 37%, rgba(' + colorRGB3 + ', ' + menu_maxBgOpacity + ') 59%, rgba(' + colorRGB4 + ', ' + menu_maxBgOpacity + ')  100%)';
                $menu.css('background', _color).addClass('bg-showed');
            }
        } 
        else {
            menu_bgShowed = false;
            var menu_bgOpacity = (scrollX / headerHeight) * menu_maxBgOpacity;
            var _color = 'linear-gradient(to right, rgba(' + colorRGB1 + ', ' + menu_maxBgOpacity + ') 0%, rgba(' + colorRGB2 + ', ' + menu_maxBgOpacity + ') 37%, rgba(' + colorRGB3 + ', ' + menu_maxBgOpacity + ') 59%, rgba(' + colorRGB4 + ', ' + menu_maxBgOpacity + ')  100%)';


            $menu.css('background', _color).addClass('bg-showed');
        }
    }
    //Авторизация
    $("#enter").click(function (e) {
        e.preventDefault();
        var user = {
            "login": $("#inputLogin").val(),
            "password": $("#inputPassword").val()
        };
        $.ajax({
            //async:false,
            url: "/api/Authentication",
            type: "POST",
            data: JSON.stringify(user),
            contentType: "application/json; charset=utf-8",
            success: function (data) {
                $('.error').addClass('text-hide');
                $('.suc').removeClass('text-hide');
                window.location.replace("/Admin/About");
            },
            error: function (XMLHttpRequest, textStatus, errorThrown) {
                $('.error').removeClass('text-hide');
        }})
        
    });
    //Выход из админа
    $("#exit").click(function () {
        $.ajax({
            url: "/api/Authentication",
            type: "GET",
            contentType: 'application/json; charset=UTF-8',
            success: function () {
                window.location.replace("/Home/Index");
            },
            error: function (e) {
                alert('При выходе возникла ошибка' + e.responseText);

            }
        })
    })
    //Perehod na panel
    $('#trans').click(function () {
        window.location.replace("/Admin/About");
    })

    //Маркировка
    var url = window.location;
    $('ul.nav a').filter(function () {
        return this.href == url;
    }).parent().addClass('active');
    $('.list-item').click(function (e) {
        $('.list-group-item').removeClass('list-group-item-info')
        $(this).parent().addClass('list-group-item-info');
    })
    //Карусель
    $('.carousel').carousel();

    ////Вход 
    //$('.modalOk').fadeOut();


    //Поиск по странице
    $('body').delegate('#spgo', 'click', function () {
        $('body,html').animate({ scrollTop: $('span.highlight:first').offset().top - paddingtop }, scrollspeed); // переход к первому фрагменту
    });

    $(window)
        // Скролл
        .scroll(function () {
            fadeMenuBg.call(this);
        }).scroll()
        // Ресайз
        .resize(function () {
            
        }).resize();
    Get();
});

function doSearchPage(text) {
    var sel;
    if (window.find && window.getSelection()) {
        sel = window.getSelection();
        if (sel.rangeCount > 0) {
            sel.collapseToEnd();
        }
        window.find(text);
    }
    else if (document.selection && document.body.createTextRange) {
        sel = document.selection;
        var textRange;
        if (sel.type == "Text") {
            textRange = sel.createRange();
            textRange.collapse(false);
        } else {
            textRange = document.body.createTextRange();
            textRange.collapse(true);
        }
        if (textRange.findText(text)) {
            textRange.select();
        }
    }
}
function searchpage() {
    doSearchPage(document.getElementById('text-search').value);
}




